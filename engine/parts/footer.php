<footer>
    <div class="container">
        <div class="padding-box">
            <div class="row">
                <div class="col-md-1">
                    <img src="/image/logo.png" class="img-responsive">
                </div>
                <div class="col-md-11">
                    <nav role="navigation" class="navbar">
                        <div id="navbarCollapse" class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left navbar-buttons">
                                <?php foreach ($app->links as $item){ ?>
                                    <li><a href="<?php print $item['url'] ?>" class="selected">
                                            <?php print $item['name'] ?>
                                        </a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </nav>

                </div>
            </div>

            <section id="counters">

            </section>
        </div>
    </div>
</footer>