<section class="dark_bg">
    <div class="padding-box  white-color">
        <div class="container">
            <p class="h1 big_zag text-center">КОНТАКТЫ</p>
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="/image/logo.png" class="mt-xlg mb-xlg">
                </div>
                <div class="col-md-offset-2 col-md-4">
                    <p class="h1 strong nomargin">
                        г. Москва
                    </p>
                    <p class="h1 strong nomargin">
                        г. Коломна
                    </p>
                    <p class="h1 strong nomargin">
                        г. Луховицы
                    </p>
                </div>
                <br>
                <div class="col-md-4">
                    <p class="h1 strong nomargin">
                        <?php print $app->phone; ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>