<?php
/**
 * Created by PhpStorm.
 * User: Denis
 * Date: 04.07.2016
 * Time: 15:09
 */
include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/App.php");

$app = new App();

if (isset($_GET['project'])){
    // project
    $id = $_GET['project'];
    $project = $app->db->get('*','projects'," WHERE `id` = '$id'");

    $images = $app->db->get("*", 'project_images', "WHERE `project_id` = '".$project->id."'");
    $project->images = array();
    is_object($images) ? array_push($project->images, $images) :  $project->images = $images;

    $includable = array("/engine/pages/project.php");
    $add_css = array('/css/jquery.fancybox.css');
    $add_js = array('/js/fancybox/jquery.fancybox.pack.js', '/js/project.js');

    include_once($_SERVER["DOCUMENT_ROOT"] . "/engine/parts/template.php");
}

else

if (isset($_GET['path'])){
    switch ($_GET['path']){
        case "contacts":
            $title = "Контакты";
            $includable = array("/engine/pages/contacts.php");
            break;
        case "404":
            $title = "404";
            $includable = array("/engine/pages/404.php");
            break;
        default:
           // header('Location: /404');
            break;
    }

    include_once($_SERVER["DOCUMENT_ROOT"] . "/engine/parts/template.php");
} else {
    // main page
    $page_class = 'mainpage';
    $title = "";
    $description = "";
    $keywords = "";
    $includable = array("/engine/pages/home.php");

   # $add_css = array('/css/works.css');
   # $add_js = array('/js/jquery.filterizr.min.js', '/js/works.js');

    include_once($_SERVER["DOCUMENT_ROOT"] . "/engine/parts/template.php");
}




?>