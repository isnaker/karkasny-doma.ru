<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="" />
    <meta name="google-site-verification" content="" />
    <meta name='wmail-verification' content='' />

    <title><?php if(isset($title)){print $title;} else {print $app->name;} ?></title>
    <?php if (isset($description)) { ?>
        <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <?php if (isset($keywords)) { ?>
        <meta name="keywords" content="<?php echo $keywords;?>"/>
    <?php } ?>

    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/reset.css" rel="stylesheet">
    <link href="/css/slick-theme.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/custombox.min.css">

    <!-- add custom css -->
    <?php if (isset($add_css)){
        foreach ($add_css as $item){ ?>
            <link rel="stylesheet" href="<?php print $item; ?>">
        <?php }
    } ?>
    <link rel="stylesheet" href="/css/responsive.css">


    <link rel="apple-touch-icon" sizes="57x57" href="/image/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/image/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/image/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/image/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/image/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/image/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/image/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/image/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/image/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/image/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/image/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="/image/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/image/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"> -->
    <!--Google analytics-->
</head>
<body>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/header.php");?>

<main class="page<?php if (isset($page_class)) { print '-'.$page_class; } ?>">
    <?php if (isset($content)){ ?>
        <?php print $content; ?>
    <?php } ?>
    <?php
    if(isset($includable)){
        foreach ($includable as $item){
            include_once($_SERVER['DOCUMENT_ROOT'].$item);
        }
    }
    ?>

    <?php  include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/footer.php");?>
</main>

<!-- callback Modal -->
</body>
<script type="text/javascript" src="/js/jquery-2.1.1.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<script type="text/javascript" src="/js/modernizr.js"></script>
<script type="text/javascript" src="/js/velocity.min.js"></script>
<script type="text/javascript" src="/js/velocity.ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/js/custombox.min.js"></script>
<!-- add custom js -->
<?php if (isset($add_js)){
    foreach ($add_js as $item){ ?>
        <script type="text/javascript" src="<?php print $item; ?>"></script>
    <?php }
} ?>
<script src="/js/script.js"></script>
</html>