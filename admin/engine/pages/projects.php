<div class="container">
    <div class="row">
        <div class="col-md-9">
            <p class="h2 mt-none">Проекты <a class="btn btn-success" href="/admin/projects/add">+ Добавить новый проект</a></p>
        </div>
    </div>
    <div class="projects">
        <div class="row">
            <?php foreach ($projects as $item) { ?>
                <div class="col-md-6">
                    <div class="item">
                        <p class="h4 mt-none"><?php print $item->name ?></p>
                        <?php if (isset($item->images) && count($item->images) > 0) { ?>
                            <?php foreach($item->images as $image){ ?>
                                <a href="/admin/index.php?action=remove_project_image&id=<?php print $image->id?>&project_id=<?php print $item->id ?>">
                                    <img width="60" class="img-thumbnail" src="/image/upload/projects/<?php print $item->id ?>/250/<?php print $image->img ?>">
                                </a>
                            <?php } ?>
                        <?php } else { ?> <p class="h6 text-center">Изображений нет</p> <?php } ?>


                        <div class="row">
                            <hr>
                            <div class="col-md-12">
                                <a class="btn btn-sm btn-primary text-uppercase" href="/admin/projects/edit/<?php print $item->id; ?>">редактировать</a>

                                <?php if ($item->hidden == 1) { ?>
                                    <a class="btn btn-sm btn-success" href="/admin/projects/show/<?php print $item->id; ?>">показать</a>
                                <?php } else { ?>
                                    <a class="btn btn-sm btn-default" href="/admin/projects/hide/<?php print $item->id; ?>">скрыть</a>
                                <?php } ?>

                                <a class="btn btn-sm btn-default pull-right" href="/admin/projects/delete/<?php print $item->id; ?>"><i class="fa fa-trash"></i> </a>

                            </div>

                        </div>
                     </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>