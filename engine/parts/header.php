<header class="navbar-fixed-top header_bg">
    <div class="container-fluid">
        <div class="container">
            <nav id="navbar_main" role="navigation" class="navbar">
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars white-color fa-2x" aria-hidden="true"></i>
                    </button>
                    <a href="/" class="navbar-brand main-color">
                        <img id="logo" src="/image/logo.png" class="img-responsive header_logo" alt="">
                    </a>
                </div>
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right navbar-buttons">
                        <?php foreach ($app->links as $item){ ?>
                            <li><a href="<?php print $item['url'] ?>" class="selected">
                                    <?php print $item['name'] ?>
                                </a></li>
                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>