<div class="container">
    <div class="row">
        <div class="col-md-1">
            <a class="btn btn-default" href="/admin">< Назад</a>
        </div>
        <div class="col-md-11">
            <?php if ( isset($project->isNew) && $project->isNew ) {?>
                <p class="h2 mt-none">Создание нового проекта</p>
            <?php } else { ?>
            <p class="h2 mt-none">Редактирование проекта "<?php print $project->name; ?>"</p>
            <?php } ?>
            <hr>
        </div>
    </div>

    <form  enctype="multipart/form-data" action="/admin/index.php" method="post">

        <?php if ( isset($project->isNew) && $project->isNew ) { ?>
            <input name="action" value="add_product" hidden="hidden">
        <?php } else { ?>
            <input name="action" value="edit_product" hidden="hidden">
            <input name="project_id" value="<?php print $project->id ?>" hidden="hidden">
        <?php } ?>

        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <div class="form-group">
                    <label>Название проекта</label>
                    <input class="form-control" name="name" value="<?php print $project->name; ?>">
                </div>
                <div class="form-group">
                    <label>Раздел</label>
                    <select class="form-control" name="category">
                        <?php foreach ($categories as $item) { ?>
                            <option
                                <?php if ($project->category_id == $item->id) { ?> selected="selected" <?php } ?>
                                value="<?php print $item->id ?>"><?php print $item->name ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Описание проекта</label>
                    <textarea rows="8" class="form-control" name="description"><?php print $project->description; ?></textarea>
                </div>
            </div>

            <div class="col-md-6 ">
                <label>Изображения в проекте</label>
                <div class="project-images well well-lg">
                    <?php if (isset($project->images) && count($project->images) > 0) { ?>
                        <?php foreach($project->images as $item){ ?>
                            <a title="Удалить изображение" class="img_remove" href="/admin/index.php?action=remove_project_image&id=<?php print $item->id?>&project_id=<?php print $project->id ?>">
                                <span class="overlay"><i class="fa fa-times"></i> </span>
                                <img width="120" class="img-thumbnail" src="/image/upload/projects/<?php print $project->id ?>/250/<?php print $item->img ?>">
                            </a>
                        <?php } ?>
                    <?php } else { ?> <p class="h6 text-center">Изображений нет</p> <?php } ?>
                </div>

               <input name="images[]" type="file" multiple />
                <button class="btn btn-primary add_more pull-right">Добавить еще фото</button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <hr>
                <button class="btn btn-primary"><i class="fa fa-save"></i> | Сохранить </button>
            </div>
        </div>
    </form>
</div>

