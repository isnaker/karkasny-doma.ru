<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 18.07.2016
 * Time: 10:53
 */
include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/DB.php");

class App {
    public $db;

    public $name = "";
    public $phone = "8 (915) 222-59-55";

    public $links = array(
       /* array('url' => "/", "name" => "Главная"),
        array('url' => "/works", "name" => "Наши работы"),
        array('url' => "/aerography", "name" => "Аэрография"),
        array('url' => "/painting", "name" => "Роспись"),
        array('url' => "/molding", "name" => "Лепнина"),
        array('url' => "/contacts", "name" => "Контакты"), */

    );

    public function __construct()
    {
        session_start();
        $this->db = new DB();
    }
}